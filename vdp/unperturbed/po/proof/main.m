
clear; 
close all;

if ~exist('mu','var')
    mu = 1;
end
load(sprintf('../newton/sol-orbit-mu_%g.mat',mu));
nu0 = '1.01';
inu = intval(nu0);

[n,d] = size(a); N = n-1;
ia = intval(a); 
iL = intval(L);
imu= intval(mu);

[f, df] = intval_zero_diff_fun( N, ia, iL, imu );
norm(f)

iA = intval(inv(mid(df)));

%% Y bound
% we perform convolution with rigorous enclousure 
ic2 = myprod3fft(N, ia(:,1), ia(:,2));
[ic2,rho] = rigencl3(ic2, ia(:,1), ia(:,1), ia(:,2));

iY = computeY( N, inu, iA * f, imu, ia, iL, ic2 ); % only the non-zero coefs

%% Z0 bound
iZ0 = computeZ0( N, inu, eye(size(iA)) - iA*df );

%% Z1 bound
% We add extra padding 
ia = [ia; zeros(500,2)]; N = N + 500;
[f, df] = intval_zero_diff_fun( N, ia, iL, imu );
iA = intval(inv(mid(df)));

% we perform convolution with rigorous enclousure 
ic2 = myprod3fft(N, ia(:,1), ia(:,2));
[ic2,rho] = rigencl3(ic2, ia(:,1), ia(:,1), ia(:,2)); 

ia1a1 = myprod2fft(N, ia(:,1), ia(:,1)); 
ia1a1 = rigencl2(ia1a1, ia(:,1), ia(:,1)); 

ia1a2 = myprod2fft(N, ia(:,1), ia(:,2));
ia1a2 = rigencl2(ia1a2, ia(:,1), ia(:,2));

iZ1 = computeZ1( N, iA, imu, ia, ia1a1, ia1a2, ic2, iL, inu );
if max(iZ1) >= 1
    warning('Z1 bigger than 1');
end

%% Z2 bound
ir = intval('1e-07');
iZ2 = computeZ2( N, iA, imu, ia, ia1a1, ia1a2, iL, inu, ir );


%% Radius polynomial solver

aa = iZ2;
bb = iZ0 + iZ1 - 1;
cc = iY; 

% rationalization of the numerator 
% r1 = (- bb - (bb.^2 - 4*aa.*cc).^.5) ./ (2*aa);
% r2 = (- bb + (bb.^2 - 4*aa.*cc).^.5) ./ (2*aa);
r1 = (-1.0 - abs(bb)./bb.*(1.0 - 4*aa.*cc./(bb.^2)).^.5) ./ (2*aa./bb);
r2 = (-1.0 + abs(bb)./bb.*(1.0 - 4*aa.*cc./(bb.^2)).^.5) ./ (2*aa./bb);
r0 = max(sup(r1))+eps;
if inf(min(r2)) < r0
    disp('invalid r0!!')
end

iY
iZ0
iZ1
iZ2
r0

save(sprintf('proof0-mu_%g.mat',mu), 'a', 'L', 'mu', 'nu0', 'r0');
