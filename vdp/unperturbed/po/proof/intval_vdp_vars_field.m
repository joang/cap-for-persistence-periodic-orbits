function [ c, dc ] = intval_vdp_vars_field( N, a, mu ) 
    % convolution
    tda1 = intval([eye(N+1) zeros(N+1)]);
    tda2 = intval([zeros(N+1) eye(N+1)]);
    [c2, dc2] = myprod3fftdif(N, a(:,1), tda1, a(:,2), tda2);
    c2 = rigencl3(c2,a(:,1),a(:,1),a(:,2));
    
    c2 = mu * (a(:,2) - c2) - a(:,1);
    
    dc2 =-mu * dc2;
    dc2(:, N+1 + (1:(N+1))) = dc2(:, N+1 + (1:(N+1))) + mu * intval(eye(N+1));
    dc2(:, 1:(N+1)) = dc2(:, 1:(N+1)) - intval(eye(N+1));
    
    c = intval([a(:,2) c2 ; 0 0 ]);
    dc = intval([tda2 zeros(N+1,1); dc2 zeros(N+1,1); zeros(1,2*(N+1)+1)]);
end

function [ c, dc ] = myprod3fftdif( N, a, da, b, db )
    % It performs a*a*b and its derivative
    
    p = (3+1)/2; % padding
    
    M1 = (2^ceil(log2(2*p*(N+1)-1))-2*p*(N+1))/2;
    Ndif = size(da,2);
    
    ta = intval([zeros(N+1+M1+1,1); flip(a(2:end)); a; zeros(N+1+M1,1)]);
    ta = verifyfft( ifftshift(ta), -1 );
    
    tda = intval([zeros(N+1+M1+1,Ndif); flip(da(2:end,:)); da; zeros(N+1+M1,Ndif)]);
    tda = verifyfft( ifftshift(tda), -1 );
        
    tb = intval([zeros(N+1+M1+1,1); flip(b(2:end)); b; zeros(N+1+M1,1)]);
    tb = verifyfft( ifftshift(tb), -1 );

    tdb = intval([zeros(N+1+M1+1,Ndif); flip(db(2:end,:)); db; zeros(N+1+M1,Ndif)]);
    tdb = verifyfft( ifftshift(tdb), -1 );
    
    
    F = real(fftshift(verifyfft(ta.*ta.*tb, 1)))*length(ta)^2;
    
    for k=1:size(tda,1)
        tda(k,:) = 2*ta(k).*tb(k).*tda(k,:) + ta(k).*ta(k).*tdb(k,:);
    end    
    tda = real(fftshift(verifyfft(tda,1)))*length(ta)^2;    

    c = F((M1+2*(N+1)+1):(M1+3*(N+1)));
    dc = tda((M1+2*(N+1)+1):(M1+3*(N+1)),:);    
end

