function c = myprod2fft( N, a, b )
    % It performs a*b
    
    p = 2; % padding
    M1 = (2^ceil(log2(2*p*(N+1)-1))-2*p*(N+1))/2;
    
    ta = intval([zeros((p-1)*(N+1)+M1+1,1); flip(a(2:end)); a; zeros((p-1)*(N+1)+M1,1)]);
    ta = verifyfft( ifftshift(ta), -1 );
    
    tb = intval([zeros((p-1)*(N+1)+M1+1,1); flip(b(2:end)); b; zeros((p-1)*(N+1)+M1,1)]);
    tb = verifyfft( ifftshift(tb), -1 );
    
    F = real( fftshift(verifyfft(ta.*tb, 1)) )*length(ta); 
    
    c = F((M1+p*(N+1)+1):(M1+2*p*(N+1)));
end