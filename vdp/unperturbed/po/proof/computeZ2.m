function Z2 = computeZ2( N, iA, imu, ia, a1a1, a1a2, iL, inu, ir )

  inuv = intval([1 2*inu.^(1:N)]);
  nrmT = 1.0 / inu + inu;
  
  lnua1 = l1nu(ia(:,1), inu);
  lnua2 = l1nu(ia(:,2), inu);
  lnua1a1 = l1nu(a1a1, inu);
  lnua1a2 = l1nu(a1a2, inu);
  
  
  iA11 = normmat( N, iA(1:(N+1),1:(N+1)), inu );
  iA12 = normmat( N, iA(1:(N+1),N+1 + (1:(N+1))), inu );
  
  iA21 = normmat( N, iA(N+1 + (1:(N+1)),1:(N+1)), inu );
  iA22 = normmat( N, iA(N+1 + (1:(N+1)),N+1 + (1:(N+1))), inu );
  
  iA31 = max(abs(iA(end,1:(N+1))) ./ inuv); 
  iA32 = max(abs(iA(end,N+1 + (1:(N+1)))) ./ inuv); 
  
  aux = (imu * (4 * lnua1a2 + 2 * lnua1a1 + ...
                6 *lnua1*ir + 3*lnua2*ir + 4*ir^2 + ...
                iL*(4*lnua1 + 2 *lnua2 + 3*ir) ...
               ) + 2*(imu+1));
  Z21 = aux * nrmT*iA12 + 2*nrmT*max(iA11,intval('1.0')/(2*(N+1)));
     
  Z22 = aux * nrmT*max(iA22,intval('1.0')/(2*(N+1))) + 2*nrmT*iA21;
  
  Z23 = aux * nrmT*iA32 + 2*nrmT*iA31;
  
  Z2 = [Z21 Z22 Z23];
end

function val = normmat(N, iA, inu)
  a = intval(zeros(1,N+1));
  for k=(N+1):-1:2
      a = a * inu + abs( iA(k,:) );
  end
  a = 2*a * inu + abs( iA(1,:) );
  inuv = intval([1 .5*inu.^-(1:N)]);
  val = max(a .* inuv);
end


