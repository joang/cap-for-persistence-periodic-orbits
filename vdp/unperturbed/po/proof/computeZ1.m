function  Z1  = computeZ1( N, iA, imu, ia, a1a1, a1a2, c2, iL, inu )
  iomgn1= 2*inu^(N+1);
  
  Z11 = l1nu(iA(1:(N+1),1), inu) + ...
        l1nu(iA(1:(N+1),N+1 + 1), inu) + ...
        iL * (l1nu(iA(1:(N+1),N+1), inu) + ...
        (imu + 1) * l1nu(iA(1:(N+1),N+1 + N+1), inu)) + ...
        2 * l1nu(iA(1:(N+1), end), inu);
  Z11 = Z11 / iomgn1;  
  Z11 = Z11 + (iL * (inu + 1/inu) + abs(ia(end, 2)) * iomgn1) / (2*(N+1));
   
  a1a2z = intval([a1a2; zeros(4*N+3 - size(a1a2,1), 1)]);
  psia1a2 = intval(zeros(N+2,1));
  for k=0:(N+1)
      psia1a2(k+1) = psifun(N, k, a1a2z, inu);
  end
  
  a1a1z = intval([a1a1; zeros(4*N+3 - size(a1a1,1), 1)]);
  psia1a1 = intval(zeros(N+2,1));
  for k=0:(N+1)
      psia1a1(k+1) = psifun(N, k, a1a1z, inu);
  end 
  
  Psi=  2*( psia1a2(1:N) + psia1a2(3:end) )+( psia1a1(1:N) + psia1a1(3:end) );
  Psi=[0; Psi];
  Z11 = Z11 + imu*iL * l1nu(abs(iA(1:(N+1), N+1 + (1:(N+1)))) * Psi, inu);
  
  
  Z12 = l1nu(iA(N+1 + (1:(N+1)),1), inu) + ...
        l1nu(iA(N+1 + (1:(N+1)),N+1 + 1), inu) + ...
        iL * (l1nu(iA(N+1 + (1:(N+1)),N+1), inu) + ...
        (imu + 1) * l1nu(iA(N+1 + (1:(N+1)),N+1 + N+1), inu)) + ...
        2*l1nu(iA(N+1 + (1:(N+1)), end), inu);
  Z12 = Z12 / iomgn1;
%       
  Z12 = Z12 + (abs(imu * ia(end,2) - ia(end,1)) * iomgn1) / (2 * (N+1));    
  
  c2 = abs(c2((N+1):(3*N+1)) - c2((N+3):(3*N+3))) ./ intval((N+1):(3*N+1))';
  c2nu = (2*inu.^((N+1):(3*N+1))) * c2;
    
  Z12 = Z12 + (iL * (imu + 1) + ...
            imu * (c2nu + iL * (2*l1nu(a1a2, inu) + l1nu(a1a1, inu))) ) ...
            * (1/inu + inu) / (2 *(N+1)); 
        
  Z12 = Z12 + imu*iL * l1nu(iA(N+1 + (1:(N+1)), N+1 + (1:(N+1))) * Psi, inu);
  
  
  Z13 = abs(iA(end,1)) + abs(iA(end,N+1 + 1)) + ...
        iL * ( abs(iA(end,N+1)) + (imu + 1) * abs(iA(end,N+1 + N+1)) ) + ...
        2* abs(iA(end,end));
  Z13 = Z13 / iomgn1;  
  Z13 = Z13 + imu*iL * abs(iA(end,N+1 + (1:(N+1)))) * Psi ;
      
  Z1 = [Z11 Z12 Z13];
end

function val = psifun(N, k, ia, inu)
  j = (N+1):(k + 2*N); % k = 0, ..., N+1
  
  val = max( abs(ia(1+abs(k-j)) + ia(1+k + j)) ./ (2 * inu.^j)' );
end
