function val = l1nu(ia, inu)
  N = length(ia);
  val = abs(ia(N));
  for k=(N-1):-1:2
      val = val * inu + abs(ia(k));
  end
  val = 2*val*inu + abs(ia(1));
end

