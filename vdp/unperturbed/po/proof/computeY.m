function iY = computeY( N, inu, ih, imu, ia, iL, ic2 )
% ic2 = a1*a1*a2
  c2 = 0.5*iL * imu * abs(ic2((N+1):(3*N+1)) - ic2((N+3):(3*N+3))) ...
      ./ intval((N+1):(3*N+1))';
  
  a = ih(1:(N+1));  
  Y1 = iL * abs(ia(end,2)) / intval(2*(N+1));
  for k=(N+1):-1:2
      Y1 = Y1 * inu + abs(a(k));
  end
  Y1 = 2 * inu * Y1 + abs(a(1));
  
  Y2 = intval('0');
  for k=(3*N+1):-1:(N+1)
      Y2 = Y2 * inu + c2(k - N);
  end
  Y2 = Y2 + iL / (2*(N+1)) * abs(imu*ia(N+1,2) - ia(N+1,1));
  a = ih(N+1 + (1:(N+1)));
  for k=(N+1):-1:2
      Y2 = Y2 * inu + abs(a(k));
  end
  Y2 = 2 * inu * Y2 + abs(a(1));
  
  Y3 = abs(ih(2*(N+1) + 1));
  iY = [Y1, Y2, Y3];
end

