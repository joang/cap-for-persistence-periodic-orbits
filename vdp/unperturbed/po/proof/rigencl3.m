function [r, rho] = rigencl3(a1a2a3, a1, a2, a3)
% It gives the rigourous enclosure of a1*a2*a3 using the Banach the algebra
  N = size(a1a2a3,1);
  aa1 = mid(a1);
  xdatain = find(abs(aa1)>1e-15);
  f = fit(xdatain,log10(abs(aa1(xdatain))), 'poly1');
  rho = 10^-f.p1;
  
  aa2 = mid(a2);
  xdatain = find(abs(aa2)>1e-15);
  f = fit(xdatain,log10(abs(aa2(xdatain))), 'poly1');
  rho = min(rho, 10^-f.p1);
  
  aa3 = mid(a3);
  xdatain = find(abs(aa3)>1e-15);
  f = fit(xdatain,log10(abs(aa3(xdatain))), 'poly1');
  rho = min(rho, 10^-f.p1);
  
  l1nua1 = l1nu(aa1, rho);
  l1nua2 = l1nu(aa2, rho);
  l1nua3 = l1nu(aa3, rho);
  
  rhok = l1nua1*l1nua2*l1nua3./[ 1 2*rho.^(1:(N-1)) ]';
  r = intersect(a1a2a3, infsup(-rhok, rhok));
end
