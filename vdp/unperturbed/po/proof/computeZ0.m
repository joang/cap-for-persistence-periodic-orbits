function Z0 = computeZ0( N, inu, iB )
  a = intval(zeros(1, 2*(N+1)));
  b = intval(zeros(1, 2*(N+1)));
  for k=(N+1):-1:2
      a = a * inu + abs( iB(k,1:(2*(N+1))) );
      b = b * inu + abs( iB(N+1 + k,1:(2*(N+1))) );
  end
  a = 2*a * inu + abs( iB(1,1:(2*(N+1))) );
  b = 2*b * inu + abs( iB(N+1 + 1,1:(2*(N+1))) );
  inuv = intval([1 .5*inu.^-(1:N)]);


  Z01 = intval('0');
  for k=(N+1):-1:2
      Z01 = Z01 * inu + abs(iB(k,end));
  end
  Z01 = 2*Z01 * inu + abs(iB(1,end));
  Z01 = Z01 + max(a(1:(N+1)) .* inuv) + max(a(N+1+ (1:(N+1))) .* inuv);

  Z02 = intval('0');
  for k=(N+1):-1:2
      Z02 = Z02 * inu + abs(iB(N+1 + k,end));
  end
  Z02 = 2*Z02 * inu + abs(iB(N+1 + 1,end));
  Z02 = Z02 + max(b(1:(N+1)) .* inuv) + max(b(N+1+ (1:(N+1))) .* inuv);
  
  Z03 = max(abs(iB(end, 1:(N+1))) .* inuv) + ...
      max(abs(iB(end, N+1 + (1:(N+1)))) .* inuv) + ...
      abs(iB(end,end));
  
  Z0 = [Z01 Z02 Z03];
end

