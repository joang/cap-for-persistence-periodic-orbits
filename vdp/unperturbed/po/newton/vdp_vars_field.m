function [ c, dc ] = vdp_vars_field( N, a, mu ) 
    % convolution
    tda1 = [eye(N+1) zeros(N+1)];
    tda2 = [zeros(N+1) eye(N+1)];
    [c2, dc2] = myprod3fftdif(N, a(:,1), tda1, a(:,2), tda2);
    
    c2 = mu * (a(:,2) - c2) - a(:,1);
    
    dc2 =-mu * dc2;
    dc2(:, N+1 + (1:(N+1))) = dc2(:, N+1 + (1:(N+1))) + mu * eye(N+1);
    dc2(:, 1:(N+1)) = dc2(:, 1:(N+1)) - eye(N+1);
    
    c = [a(:,2) c2 ; 0 0 ];
    dc = [tda2 zeros(N+1,1); dc2 zeros(N+1,1); zeros(1,2*(N+1)+1)];
end

function [ c, dc ] = myprod3fftdif( N, a, da, b, db )
    % It performs a*a*b and its derivative by the use of FFT
    
    p=3;
    q=(p+2-mod(p,2))/2;
    
    Ndif = size(da,2);    
    
    ta = [zeros((q-1)*(N+1),1); flip(a(2:end)); a; zeros((q-1)*(N+1),1)];
    ta = ifft(ifftshift(ta));
    
    tda = [zeros((q-1)*(N+1),Ndif); flip(da(2:end,:)); da; zeros((q-1)*(N+1),Ndif)];
    tda = ifft(ifftshift(tda));
    
    
    tb = [zeros((q-1)*(N+1),1); flip(b(2:end)); b; zeros((q-1)*(N+1),1)];
    tb = ifft(ifftshift(tb));

    tdb = [zeros((q-1)*(N+1),Ndif); flip(db(2:end,:)); db; zeros((q-1)*(N+1),Ndif)];
    tdb = ifft(ifftshift(tdb));
    
    
    F = real(fftshift(fft(ta.*ta.*tb)))*length(ta)^(p-1);
    
    for k=1:length(tda)
        tda(k,:) = 2*ta(k).*tb(k).*tda(k,:) + ta(k).*ta(k).*tdb(k,:);
    end    
    tda = real(fftshift(fft(tda)))*length(ta)^(p-1);
    
    c  = F((q*(N+1)):((q+1)*(N+1)-1));
    dc = tda(q*(N+1):((q+1)*(N+1)-1), :);
end

