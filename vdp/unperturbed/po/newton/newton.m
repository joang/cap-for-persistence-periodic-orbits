function [ x, flag ] = newton( fun, x, varargin )  
  if nargin > 2
      tol=varargin{1};
  else
      tol=1.0e-13;
  end
  if nargin > 3
      fileID=varargin{2};
  else
      fileID=1;
  end
  
  for iter=1:15
     [ f, df ] = fun(x);
     err = norm(f);
     
     % linear solver
     b = df\f;
     
     % correction
     x = x - b;
     
     if fileID ~= 0
         fprintf(fileID, '%4d % .5e % .5e % .5e\n', iter, err, norm(b), cond(df));
     end
     if err < tol
         flag = 0;
         return;
     end
  end
  flag = 1;
end

