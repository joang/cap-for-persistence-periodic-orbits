function dydt = vdp(~,y,mu,T)
    % rescaled vdp equation with period 1
    dydt =[y(2); mu*(1-y(1)^2)*y(2)-y(1)]*T;
end
