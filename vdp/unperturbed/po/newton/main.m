
clear;

% Number of Chebyshev coefficients
if ~exist('N','var')
  N = 200;
end

% Model parameter value
if ~exist('mu','var')
    mu = 1;
end

% Initial seeds. The should depend on the parameter mu
% In practice we use the same all the time.
y = [0; 2.172713692622530];
L = 6.663286859323093/2;

% Integration by chebfun
K = chebfun.ode113(@(t,x) vdp(t,x,mu,L), [-1,1], y(1:2), ...
                   odeset('RelTol',1e-14,'AbsTol',1e-14));
                   
% Chebyshev coefficients
a = chebcoeffs(K,N+1,'kind',1);
a(2:end, :) = a(2:end, :)/2;

% Newton solver on the Chebyshev coefficients of the model
n = size(a,1);
x = newton(@(x) zero_diff_fun(n-1,reshape(x(1:end-1), [n,2]), x(end),mu), ...
    [a(:,1); a(:,2); L], 1.0e-13);

% Saving of the periodic orbit and its period
a = reshape(x(1:end-1), [n,2]);
L = x(end);
save(sprintf('sol-orbit-mu_%g.mat',mu), 'mu', 'a', 'L')
