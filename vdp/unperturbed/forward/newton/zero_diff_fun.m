function [f,df] = zero_diff_fun( a, L, mu, v, p )
  n = length(a)/2;
  
  R = zeros(n);
  R(1,1) = -1;
  R(1,2:n) = 2*(2*mod(1:(n-1),2)-ones(1,n-1));
  
  Lam = 2*diag((1:n)-1);
  
  T = diag([0;ones(n-2,1)],1)-diag(ones(n-1,1),-1);
  
  a1 = a(1:n);
  a2 = a(n+(1:n));
  
  v1 = v(1:n);
  v2 = v(n+(1:n));
  
  [c1, dc1] = myprod3fftdiff(a1,a2,v1, eye(n));
  c1  = -2*mu*c1 - v1;
  dc1 = -2*mu*dc1 - eye(n);
  
  [c2, dc2] = myprod3fftdiff(a1,a1,v2, eye(n));
  c2  = mu*(v2 - c2);
  dc2 = mu*(eye(n) - dc2);
  
  f = p+[(R+Lam)*v1;(R+Lam)*v2] + L*[T*v2;T*(c1+c2)];

  df = [ R+Lam        L*T; ...
        L*T*dc1 R+Lam+L*T*dc2];
end
