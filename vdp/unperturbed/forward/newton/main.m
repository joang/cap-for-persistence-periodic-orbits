
% clear;

if ~exist('mu','var')
    mu = 1;
end

load(sprintf('../../po/newton/sol-orbit-mu_%g.mat',mu));

n = size(a,1);

%initial seed
e = [ 1; zeros(n-1,1); zeros(n,1) ]; 
v = e;
s1 = newton(@(x) zero_diff_fun(a(:),L,mu,x,e), v, 1.0e-13);
% s1 = [ v11; v21 ]

%initial seed
e = [ zeros(n,1); 1; zeros(n-1,1) ]; 
v = e;
s2 = newton(@(x) zero_diff_fun(a(:),L,mu,x,e), v, 1.0e-13);
% s2 = [ v12; v22]

% v(k,:) = [v11(k) v21(k) v12(k) v22(k) ]
v = [reshape(s1,[n,2]) reshape(s2,[n,2])];
save(sprintf('sol-fwd-mu_%g.mat',mu), 'v');

% g = check_vars(201,1.0e-2,[a1;a2], L, mu, v, e); norm(g)

