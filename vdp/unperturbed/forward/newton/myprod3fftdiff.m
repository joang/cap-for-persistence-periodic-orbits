function [ c, dc ] = myprod3fftdiff(a1,a2,a3, db)
  N = length(a1)-1;
  p=3;
  q=(p+2-mod(p,2))/2;
    
  ta1 = [zeros((q-1)*(N+1),1); flip(a1(2:end)); a1; zeros((q-1)*(N+1),1)];
  ta1 = ifft(ifftshift(ta1));
    
  ta2 = [zeros((q-1)*(N+1),1); flip(a2(2:end,:)); a2; zeros((q-1)*(N+1),1)];
  ta2 = ifft(ifftshift(ta2));
     
  ta3 = [zeros((q-1)*(N+1),1); flip(a3(2:end,:)); a3; zeros((q-1)*(N+1),1)];
  ta3 = ifft(ifftshift(ta3));
    
  tdb = [zeros((q-1)*(N+1),N+1); flip(db(2:end,:)); db; zeros((q-1)*(N+1),N+1)];
  tdb = ifft(ifftshift(tdb));
    
  F = real(fftshift(fft(ta1.*ta2.*ta3)))*length(ta1)^(p-1);
     
  for k=1:length(ta2)
      tdb(k,:) = ta1(k).*ta2(k).*tdb(k,:);
  end 
    
  tdb = real(fftshift(fft(tdb)))*length(ta1)^(p-1);
    
  c = F((q*(N+1)):((q+1)*(N+1)-1),:);
  dc = tdb(q*(N+1):((q+1)*(N+1)-1), :); 
end