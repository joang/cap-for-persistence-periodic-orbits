function [f, df] = intval_zero_diff_fun_fwd(N, ia, iL, imu, iv, ip)
% n=N+1+1 because of the tridiagonal operator
n=N+2;

iv1 = iv(:,1);
iv2 = iv(:,2);

R=intval(zeros(n));
R(1,1)=intval('-1');
R(1,2:n)=intval(2*(2*mod(1:(n-1),2)-ones(1,n-1)));

Lam=intval(2*diag((1:n)-1));

T=intval(diag([0;ones(n-2,1)],1)-diag(ones(n-1,1),-1));

% ialpha=2*imu*myprod2fftint(N,ia(:,1),ia(:,2));
% ialpha(1)=ialpha(1)-intval(1);
% ibeta=-imu*myprod2fftint(N,ia(:,1),ia(:,1));
% ibeta(1)=ibeta(1)+imu;

%zero function f
f=intval(zeros(n+3*N+2,1));
%first component 0,1,...,N+1
f(1:n)=[ip(:,1);0]+(R+Lam)*[iv1; 0]+iL*T*[iv2;0];

%second component 0,1,...,3N+1
%convolution with alpha
[c1, dc1]= myprod3fftdiffint(N, ia(:,1), ia(:,2),iv1,intval(eye(N+1)));
%rigorous enclosure for convolution %with or without not much difference
[c1,rho] = rigencl3(3*N+1, c1, ia(:,1), ia(:,2), iv1);
c1=-2*imu*c1-[iv1;zeros(2*N,1)]; %0,1,...,3N
dc1=-2*imu*dc1-eye(N+1);

%convolution with beta
[c2, dc2]= myprod3fftdiffint(N,ia(:,1),ia(:,1),iv2,intval(eye(N+1)));
[c2,rho] = rigencl3(3*N+1, c2, ia(:,1), ia(:,1), iv2);
c2=imu*[iv2;zeros(2*N,1)]-imu*c2;
dc2=imu*eye(N+1)-imu*dc2;
%
T2=intval(diag([0;ones(3*N,1)],1)-diag(ones(3*N+1,1),-1));
%0,1,...,3N+1
f((n+1):end)= iL*T2*([c1;0]+[c2;0]);
f((n+1):2*n)= f((n+1):2*n)+[ip(:,2);0]+(R+Lam)*[iv2;0];
%Df
df=[R(1:(N+1),1:(N+1))+Lam(1:(N+1),1:(N+1)) iL*T(1:(N+1),1:(N+1));
    iL*T(1:(N+1),1:(N+1))*dc1 R(1:(N+1),1:(N+1))+Lam(1:(N+1),1:(N+1))+iL*T(1:(N+1),1:(N+1))*dc2];
end