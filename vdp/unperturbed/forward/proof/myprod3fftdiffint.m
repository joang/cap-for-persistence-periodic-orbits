function[c, dc] = myprod3fftdiffint( N, a1, a2, a3 ,dd )
    % It performs a*a*b
    
    p = 3; % padding
    M1 = (2^ceil(log2(2*p*(N+1)-1))-2*p*(N+1))/2;    
    
    ta1 = intval([zeros((p-1)*(N+1)+M1+1,1); flip(a1(2:end)); a1; zeros((p-1)*(N+1)+M1,1)]);
    ta1 = verifyfft( ifftshift(ta1),-1 );
%     ze = intval(zeros(size(ta)));
%     ta = intval_fft( ifftshift(ta), ze, -1 );
     
    ta2 = intval([zeros((p-1)*(N+1)+M1+1,1); flip(a2(2:end)); a2; zeros((p-1)*(N+1)+M1,1)]);
    ta2 = verifyfft( ifftshift(ta2),-1 );
%     tb = intval_fft(ifftshift(tb), ze, -1 ); 

    ta3 = intval([zeros((p-1)*(N+1)+M1+1,1); flip(a3(2:end)); a3; zeros((p-1)*(N+1)+M1,1)]);
    ta3 = verifyfft( ifftshift(ta3),-1 );

    td = intval([zeros((p-1)*(N+1)+M1+1,N+1); flip(dd(2:end,:)); dd; zeros((p-1)*(N+1)+M1,N+1)]);
    td = verifyfft(ifftshift(td), -1);
    
    F = real( fftshift(verifyfft(ta1.*ta2.*ta3)) );    
%     F = fftshift(intval_fft(ta.*ta.*tb, ze, 1) / size(ze,1));
    
    for k=1:length(ta2)
        td(k,:) = ta1(k).*ta2(k).*td(k,:);
    end 
    
    td = real(fftshift(verifyfft(td)));
    
    c = F((M1+p*(N+1)+1):(M1+2*p*(N+1)-(p-1)))*length(ta1)^(p-1);
    %what should be the dimension of dc?
    dc= td((M1+p*(N+1)+1):(M1+(p+1)*(N+1)),:)*length(ta1)^(p-1);
end