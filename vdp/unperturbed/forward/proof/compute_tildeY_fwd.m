function itildeY = compute_tildeY_fwd(inu, iA, iL, ia, imu, iv, ir0)
[n,d] = size(ia);
ia = [ia; zeros(1,d)];

[~,d] = size(iv);
iv = [iv; zeros(1,d)];

alphv1 = conv3mat(ia(:,1),ia(:,2),iv(:,1));
alphv1 =-2*imu*alphv1 - iv(:,1);

betav2 = conv3mat(ia(:,1),ia(:,1),iv(:,2));
betav2 = imu*(iv(:,2)-betav2);

a1v1 = conv2mat(ia(:,1),iv(:,1));
a1v2 = conv2mat(ia(:,1),iv(:,2));
a2v1 = conv2mat(ia(:,2),iv(:,1));

T=intval(diag([0;ones(n-1,1)],1)-diag(ones(n,1),-1));
iA11T = [iA(  (1:n),  (1:n)) zeros(n,1); zeros(1,n+1)]*T;
iA12T = [iA(  (1:n),n+(1:n)) zeros(n,1); zeros(1,n+1)]*T;
iA21T = [iA(n+(1:n),  (1:n)) zeros(n,1); zeros(1,n+1)]*T;
iA22T = [iA(n+(1:n),n+(1:n)) zeros(n,1); zeros(1,n+1)]*T;

tildeY1 = ir0*l1nu(iA12T*alphv1 + iA11T*iv(:,2) + iA12T*betav2,inu) +...
    2*abs(imu)*(iL + ir0)*ir0*(...
    l1nu(iA11T*a1v1,inu) + ...
    l1nu(iA11T*a2v1,inu) + ...
    l1nu(iA12T*a1v2,inu) + ...
    (l1nu(iv(:,1),inu) + 0.5*l1nu(iv(:,2),inu))*ir0);

tildeY2 = ir0*l1nu(iA22T*alphv1 + iA21T*iv(:,2) + iA22T*betav2,inu) +...
    2*abs(imu)*(iL + ir0)*ir0*(...
    l1nu(iA21T*a1v1,inu) + ...
    l1nu(iA21T*a2v1,inu) + ...
    l1nu(iA22T*a1v2,inu) + ...
    (l1nu(iv(:,1),inu) + 0.5*l1nu(iv(:,2),inu))*ir0);

itildeY = [ tildeY1, tildeY2 ];
end

function [ab, A] = conv2mat(a,b)
 m = length(a); 
 A = toeplitz(a) + [zeros(m-1,1) hankel(a(2:end)); zeros(1,m)];
 ab= A*b;
end

function [abc, AB] = conv3mat(a,b,c)
 n = length(a);
 a = [a; zeros(n,1)];
 b = [b; zeros(n,1)];
 c = [c; zeros(n,1)];
 
 m = length(a);
 A = toeplitz(a) + [zeros(m-1,1) hankel(a(2:end)); zeros(1,m)];
 B = toeplitz(b) + [zeros(m-1,1) hankel(b(2:end)); zeros(1,m)];
 AB = A*B;
 abc= AB*c;
 
 AB = AB(1:n,1:n);
 abc= abc(1:n);
end
