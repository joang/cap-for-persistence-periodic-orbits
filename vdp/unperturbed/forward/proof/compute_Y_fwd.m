function Y = compute_Y_fwd(N, inu, f, A)

n=N+1;
wei1=intval(ones(n+1,1));
wei1(2:end)=2*inu.^(1:n);

% dim of f_1 is N+2, dim of f_2 is 3N+2

% first component of Y
% first n entries of f1 and first n entries of f2
b=intval(zeros(n+1,1));
b(1:n)=A(1:n,1:n)*f(1:n)+A(1:n,n+(1:n))*f((n+2):(2*n+1));
% add the term coming from operator T
b(n+1)=f(n+1)/(2*n); % CANNOT USE (1/(2*n))*f(n+1)!!!
% nu norm of b
Y1=abs(b)'*wei1;

% second component of Y
b=intval(zeros(3*N+2,1));
% finite part
b(1:n)=A(n+(1:n),1:n)*f(1:n)+A(n+(1:n),n+(1:n))*f((n+2):(2*n+1));
% tail part coming from convolution and operator T
b(n+1:end)=f((2*n+2):end)./(2*(n:(3*N+1)))';

wei2=intval(ones(3*N+2,1));
wei2(2:end,1)=2*inu.^(1:3*N+1);
% nu norm of b
Y2=abs(b)'*wei2;

Y=[ Y1, Y2 ];
end
