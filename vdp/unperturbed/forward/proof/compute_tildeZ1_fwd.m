function itildeZ1 = compute_tildeZ1_fwd(inu, iA, iL, ia, imu, ir0)
n = size(ia,1); N = n-1;
% tail
l1a1 = l1nu(ia(:,1),inu);
l1a2 = l1nu(ia(:,2),inu);
alph = 2*abs(imu)*l1a1*l1a2 + 1;
beta =   abs(imu)*(l1a1*l1a1 + 1);
tildealph = 2*abs(imu)*ir0*(l1a1 + l1a2 + ir0);
tildebeta =   abs(imu)*ir0*(2*l1a1 + ir0);
alph = alph + tildealph;
beta = beta + tildebeta;

T = intval(diag([0;ones(3*N,1)],1)-diag(ones(3*N+1,1),-1));
wei1 = [1; 2*inu.^(1:(3*N+1))'];
l1A11 = normm(iA(        (1:(3*N+2)),        (1:(3*N+2)))*T, wei1);
l1A12 = normm(iA(        (1:(3*N+2)),(3*N+2)+(1:(3*N+2)))*T, wei1);
l1A21 = normm(iA((3*N+2)+(1:(3*N+2)),        (1:(3*N+2)))*T, wei1);
l1A22 = normm(iA((3*N+2)+(1:(3*N+2)),(3*N+2)+(1:(3*N+2)))*T, wei1);

tildeZ1 = ir0*(max(l1A12*alph, l1A11 + l1A12*beta)) + ...
          iL*l1A12*(tildealph + tildebeta);
tildeZ2 = ir0*(max(l1A22*alph, l1A21 + l1A22*beta)) + ...
          iL*l1A22*(tildealph + tildebeta);
      
itildeZ1 = [ tildeZ1 tildeZ2 ];
end



function norm=normm(iB, wei)
    norm = max(abs(iB')*wei./wei);
end
