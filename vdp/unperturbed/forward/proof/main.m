% clear;
if ~exist('mu','var')
    mu = 1;
end
load(sprintf('../../po/proof/proof0-mu_%g.mat',mu));
load(sprintf('../newton/sol-fwd-mu_%g.mat',mu));
nu1 = '1.01';
inu = intval(nu1);
ir0 = intval(r0);

inu0= intval(nu0);

[n,d] = size(a); N = n-1; 
ia = intval(a);
iL = intval(L);
imu= intval(mu);

iv = intval(v);

one = intval([1; zeros(N,1)]);
zer = intval(zeros(n,1));

[ir11, iY1, iZ01, iZ11,nrmf1] = proof_fwd(N, inu, ia, iL, imu, ir0, iv(:,[1 2]), [one zer])

[ir12, iY2, iZ02, iZ12,nrmf2] = proof_fwd(N, inu, ia, iL, imu, ir0, iv(:,[3 4]), [zer one])

ir1 = max(ir11,ir12);
r1 = mid(ir1)

save(sprintf('proof1-mu_%g.mat',mu), 'v', 'nu1', 'r1');

