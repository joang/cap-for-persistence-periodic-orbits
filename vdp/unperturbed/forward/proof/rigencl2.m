function [r, rho] = rigencl2(N, a1a2, a1, a2)
% It gives the rigourous enclosure of a1*a2 using the Banach the algebra

  aa1 = mid(a1);
  xdatain = find(abs(aa1)>1e-16);
  f = fit(xdatain,log10(abs(aa1(xdatain))), 'poly1');
  rho = 10^-f.p1;
  
  aa2 = mid(a2);
  xdatain = find(abs(aa2)>1e-16);
  f = fit(xdatain,log10(abs(aa2(xdatain))), 'poly1');
  rho = min(rho, 10^-f.p1);
  
  l1nua1 = l1nu(aa1, rho);
  l1nua2 = l1nu(aa2, rho);
  
  rhok = l1nua1*l1nua2./[ 1 2*rho.^(1:(N-1)) ]';
  r = intersect(a1a2, infsup(-rhok, rhok));
end
