function [r, rho] = rigencl3d(M, a1a2a3, a1, a2, a3)
% It gives the rigourous enclosure of a1*a2*a3 using the Banach the algebra

  aa1 = mid(a1);
  xdatain = find(abs(aa1)>1e-16);
  f = fit(xdatain,log10(abs(aa1(xdatain))), 'poly1');
  rho = 10^-f.p1;
  
  aa2 = mid(a2);
  xdatain = find(abs(aa2)>1e-16);
  f = fit(xdatain,log10(abs(aa2(xdatain))), 'poly1');
  rho = min(rho, 10^-f.p1);
  
  aa3 = mid(a3);
  
  l1nua1 = l1nu(aa1, rho);
  l1nua2 = l1nu(aa2, rho);

 for i=1:M
  
  l1nua3 = l1nu(aa3(:,i), rho);
  
  rhok = l1nua1*l1nua2*l1nua3./[ 1 2*rho.^(1:(M-1)) ]';
  r(:,i) = intersect(a1a2a3(:,i), infsup(-rhok, rhok));
 end
end
