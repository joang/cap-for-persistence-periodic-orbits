function [f,df] = zero_diff_fun( a, L, mu, u, p )
  n = length(a)/2;
  
  R = zeros(n);
  R(1,1) = -1;
  R(1,2:n) = 2*(2*mod(1:(n-1),2)-ones(1,n-1));
  
  Lam = 2*diag((1:n)-1);
  
  T = diag([0;ones(n-2,1)],1)-diag(ones(n-1,1),-1);
  
  a1 = a(1:n);
  a2 = a(n+(1:n));
  
  u1 = u(1:n);
  u2 = u(n+(1:n));
  
  [c1, dc1] = conv3mat(a1,a2,u2);% myprod3fftdiff(a1,a2,u2, eye(n)); %
  c1  = -2*mu*c1 - u2;
  dc1 = -2*mu*dc1 - eye(n);
  
  [c2, dc2] = conv3mat(a1,a1,u2);% myprod3fftdiff(a1,a1,u2, eye(n)); %
  c2  = mu*(u2 - c2);
  dc2 = mu*(eye(n) - dc2);
  
  f = p+[(R+Lam)*u1;(R+Lam)*u2] - L*[T*c1;T*(u1+c2)];

  df = [  R+Lam       -L*T*dc1; ...
          -L*T   R+Lam-L*T*dc2];
end

function [abc, AB] = conv3mat(a,b,c)
 n = length(a);
 a = [a; zeros(n,1)];
 b = [b; zeros(n,1)];
 c = [c; zeros(n,1)];
 
 m = length(a);
 A = toeplitz(a) + [zeros(m-1,1) hankel(a(2:end)); zeros(1,m)];
 B = toeplitz(b) + [zeros(m-1,1) hankel(b(2:end)); zeros(1,m)];
 AB = A*B;
 abc= AB*c;
 
 AB = AB(1:n,1:n);
 abc= abc(1:n);
end
