
% clear;
if ~exist('mu','var')
    mu = 1;
end
load(sprintf('../../po/newton/sol-orbit-mu_%g.mat',mu));
[n,d] = size(a);


%initial seed
e = [ 1; zeros(n-1,1); zeros(n,1) ]; 
u = e;
% norm(check_vars(n+8, @(x) zero_diff_fun(a(:),L,mu,x,e), u, 1.0e-5))
[s1, flag] = newton(@(x) zero_diff_fun(a(:),L,mu,x,e), u, 1.0e-11);
assert(flag==0);
% s1 = [u11; u12]

%initial seed
e = [ zeros(n,1); 1; zeros(n-1,1) ]; 
u = e;
[s2, flag] = newton(@(x) zero_diff_fun(a(:),L,mu,x,e), u, 1.0e-11);
assert(flag==0);
% s2 = [ u21 ; u22 ]

% u(k,:) = [ u11(k) u12(k) u21(k) u22(k) ]
u = [reshape(s1,n,d) reshape(s2,n,d)];
save(sprintf('sol-bwd-mu_%g.mat',mu), 'u');