function [ir1, iY, iZ0, iZ1, nrmf] = proof_bwd(N, inu, ia, iL, imu, ir0, iu, ip)
 
  [f, df] = intval_zero_diff_fun_bwd(N, ia, iL, imu , iu, ip);

  nrmf = norm(f);

  % inverse of df dim 2n*2n
  iA = intval(inv(mid(df)));

  %% Y bound
  ibY = compute_Y_bwd(N, inu, f, iA);
  itildeY = compute_tildeY_bwd(inu, iA, iL, ia, imu, iu, ir0);
  iY = ibY + itildeY;

  %% Z0 bound
  iZ0 = compute_Z0_bwd(N, inu, eye(size(iA)) - iA*df);


  %% Z1 bound
  % We add extra padding 
  d = 2; padding=900;
  ia = intval([ia; zeros(padding,d)]);
  iu = intval([iu; zeros(padding,d)]);
  ip = intval([ip; zeros(padding,d)]);
  N = N + padding;

  [~, df] = intval_zero_diff_fun_bwd(N, ia, iL, imu , iu, ip);
  iA = intval(inv(mid(df)));


  alpha=conv2mat([ia(:,1); zeros(N,1)], [ia(:,2); zeros(N,1)]);
  alpha=-2*imu*alpha;
  alpha(1)=alpha(1)-1;
  
  beta=conv2mat([ia(:,1); zeros(N,1)], [ia(:,1); zeros(N,1)]);
  beta=-imu*beta;
  beta(1)=beta(1)+imu;
  
  %long derivative
  %derivative of alpha convolution
  a=[alpha;zeros(2*N+1,1)];
  dc1=toeplitz(a)...
      +[zeros(4*N+1,1) hankel(a(2:end)); zeros(1,4*N+2)];
  %derivative of beta convolution
  a=[beta;zeros(2*N+1,1)];
  dc2=toeplitz(a)...
      +[zeros(4*N+1,1) hankel(a(2:end)); zeros(1,4*N+2)];

  [ibarZ1,iA] = compute_Z1_bwd( N, inu, iL, dc1, dc2, df, iA, alpha,beta);
  itildeZ1 = compute_tildeZ1_bwd(inu, iA, dc1, dc2, iL, ia, imu, ir0);
  iZ1 = ibarZ1 + itildeZ1;
  assert(max(iZ1) < 1);

  ir1 = -iY/(iZ0+iZ1-1) + 1.0e-15;
end

