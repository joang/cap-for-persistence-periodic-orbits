function Y = compute_Y_bwd(N, inu, f, A)
  n=N+1;
  m=3*N+2;
  % dim of f_1 is N+2, dim of f_2 is 3N+2

  b=intval(zeros(m,1));
  % first component of Y
  % first n entries of f1 and first n entries of f2
  b(1:n)=A(1:n,1:n)*f(1:n)+A(1:n,n+(1:n))*f((m+1):(m+n));
  % add the term coming from operator T
  b((n+1):end)=f((n+1):m)./(2*(n:(m-1)))';
  % nu norm of b
  Y1=l1nu(b,inu);

  % second component of Y
  % finite part
  b(1:n)=A(n+(1:n),1:n)*f(1:n)+A(n+(1:n),n+(1:n))*f((m+1):(m+n));
  % tail part coming from convolution and operator T
  b(n+1:end)=f((m+n+1):end)./(2*(n:(m-1)))';

  % nu norm of b
  Y2=l1nu(b,inu);

  Y=[ Y1, Y2 ];
end
