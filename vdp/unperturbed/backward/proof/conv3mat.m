function [abc, AB] = conv3mat(a,b,c)
 n = length(a);
 a = [a; zeros(n,1)];
 b = [b; zeros(n,1)];
 c = [c; zeros(n,1)];
 
 m = length(a);
 A = toeplitz(a) + [zeros(m-1,1) hankel(a(2:end)); zeros(1,m)];
 B = toeplitz(b) + [zeros(m-1,1) hankel(b(2:end)); zeros(1,m)];
 AB = A*B;
 abc= AB*c;
 
 AB = AB(1:n,1:n);
 abc= abc(1:n);
end

