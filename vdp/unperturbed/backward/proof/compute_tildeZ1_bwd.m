function itildeZ1 = compute_tildeZ1_bwd(inu, iA, alph, beta, iL, ia, imu, ir0)
  n = size(ia,1); N = n-1;

  T = intval(diag([0;ones(4*N,1)],1)-diag(ones(4*N+1,1),-1));

  iA11T = iA(        (1:(4*N+2)),        (1:(4*N+2)))*T;
  iA12T = iA(        (1:(4*N+2)),(4*N+2)+(1:(4*N+2)))*T;
  iA21T = iA((4*N+2)+(1:(4*N+2)),        (1:(4*N+2)))*T;
  iA22T = iA((4*N+2)+(1:(4*N+2)),(4*N+2)+(1:(4*N+2)))*T;

  wei1 = [1; 2*inu.^(1:(4*N+1))'];

  [~, A1] = conv2mat([ia(:,1);zeros(3*N+1,1)], [ia(:,1);zeros(3*N+1,1)]);
  [~, A2] = conv2mat([ia(:,2);zeros(3*N+1,1)], [ia(:,2);zeros(3*N+1,1)]);

  tildeZ1 = ir0*normm(iA12T + iA11T*alph + iA12T*beta,wei1);
  tildeZ1 = tildeZ1 + (iL + ir0)*2*abs(imu)*ir0*(...
      normm(iA11T*A1,wei1) + ...
      normm(iA11T*A2,wei1) + ...
      normm(iA12T*A1,wei1) + ...
      1.5*ir0 );

  tildeZ2 = ir0*normm(iA22T + iA21T*alph + iA22T*beta,wei1);
  tildeZ2 = tildeZ2 + (iL + ir0)*2*abs(imu)*ir0*(...
      normm(iA21T*A1,wei1) + ...
      normm(iA21T*A2,wei1) + ...
      normm(iA22T*A1,wei1) + ...
      1.5*ir0 );
        
  itildeZ1 = [ tildeZ1 tildeZ2 ];
end

function norm=normm(iB, wei)
  norm = max(abs(iB')*wei./wei);
end
