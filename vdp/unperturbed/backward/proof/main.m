

if ~exist('mu','var')
    mu = 1;
end
load(sprintf('../../po/proof/proof0-mu_%g.mat',mu));
load(sprintf('../newton/sol-bwd-mu_%g.mat',mu));
nu2 = '1.01';
inu = intval(nu2);
ir0 = intval(r0);

inu0= intval(nu0);

[n,d] = size(a); N = n-1;
ia = intval(a); 
iL = intval(L);
imu= intval(mu);

iu = intval(u);
iu((n-25):n,2:2:end)=0; % we neglect the bump at the end

one = intval([1; zeros(N,1)]);
zer = intval(zeros(n,1));

[ir21, iY1, iZ01, iZ11,nrmf1] = proof_bwd(N, inu, ia, iL, imu, ir0, iu(:,[1 2]), [one zer])

[ir22, iY2, iZ02, iZ12,nrmf2] = proof_bwd(N, inu, ia, iL, imu, ir0, iu(:,[3 4]), [zer one])

ir2 = max(ir21,ir22);
r2 = mid(ir2)

save(sprintf('proof2-mu_%g.mat',mu), 'u', 'nu2', 'r2');

