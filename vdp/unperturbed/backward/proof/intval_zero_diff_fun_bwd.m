function [f, df] = intval_zero_diff_fun_bwd(N, ia, iL, imu, iu, ip)
[n,d] = size(ia);
ia = [ia; zeros(2*N+1,d)];
[~,d] = size(iu);
iu = [iu; zeros(2*N+1,d)];
[~,d] = size(ip);
ip = [ip; zeros(2*N+1,d)];

% n=3*N+2;
n=n+2*N+1;

iu1 = iu(:,1);
iu2 = iu(:,2);

R=intval(zeros(n));
R(1,1)=intval('-1');
R(1,2:n)=intval(2*(2*mod(1:(n-1),2)-ones(1,n-1)));

Lam=intval(2*diag((1:n)-1));

T=intval(diag([0;ones(n-2,1)],1)-diag(ones(n-1,1),-1));

% ialpha=2*imu*myprod2fftint(N,ia(:,1),ia(:,2));
% ialpha(1)=ialpha(1)-intval(1);
% ibeta=-imu*myprod2fftint(N,ia(:,1),ia(:,1));
% ibeta(1)=ibeta(1)+imu;

%convolution with alpha
[c1,dc1]=conv3mat(ia(:,1),ia(:,2),iu2);
c1=-2*imu*c1-iu2; %0,1,...,3N
dc1=-2*imu*dc1-eye(n);
% [c1, dc1]= myprod3fftdiffint(N, ia(:,1), ia(:,2),iu2,intval(eye(N+1)));
%rigorous enclosure for convolution %with or without not much difference
% [c1,rho] = rigencl3(3*N+1, c1, ia(:,1), ia(:,2), iu2);
% c1=-2*imu*c1-[iu2;zeros(2*N,1)]; %0,1,...,3N
% dc1=-2*imu*dc1-eye(N+1);

%zero function f
f=intval(zeros(2*n,1));
%first component 0,1,...,3N+2
f(1:n)=ip(:,1)+(R+Lam)*iu1-iL*T*c1;

%second component 0,1,...,3N+1

%convolution with beta
[c2,dc2]=conv3mat(ia(:,1),ia(:,1),iu2);
c2=imu*(iu2-c2); %0,1,...,3N
dc2=imu*(eye(n)-dc2);
% [c2, dc2]= myprod3fftdiffint(N,ia(:,1),ia(:,1),iu2,intval(eye(N+1)));
% [c2,rho] = rigencl3(3*N+1, c2, ia(:,1), ia(:,1), iu2);
% c2=imu*[iu2;zeros(2*N,1)]-imu*c2;
% dc2=imu*eye(N+1)-imu*dc2;
%0,1,...,3N+1
f((n+1):end) = ip(:,2) + (R+Lam)*iu2 - iL*T*(iu1+c2);
%Df
df=[R(1:(N+1),1:(N+1))+Lam(1:(N+1),1:(N+1)) -iL*T(1:(N+1),1:(N+1))*dc1(1:(N+1),1:(N+1));
    -iL*T(1:(N+1),1:(N+1)) R(1:(N+1),1:(N+1))+Lam(1:(N+1),1:(N+1))-iL*T(1:(N+1),1:(N+1))*dc2(1:(N+1),1:(N+1))];
end