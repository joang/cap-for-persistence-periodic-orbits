function itildeY = compute_tildeY_bwd(inu, iA, iL, ia, imu, iu, ir0)
  [n,d] = size(ia);
  ia = [ia; zeros(1,d)];
  [~,d] = size(ia);
  iu = [iu; zeros(1,d)];

  alphu2 = conv3mat(ia(:,1),ia(:,2),iu(:,2));
  alphu2 =-2*imu*alphu2 - iu(:,2);

  betau2 = conv3mat(ia(:,1),ia(:,1),iu(:,2));
  betau2 = imu*(iu(:,2) - betau2);

  T=intval(diag([0;ones(n-1,1)],1)-diag(ones(n,1),-1));

  iA11T = [iA(  (1:n),  (1:n)) zeros(n,1); zeros(1,n+1)]*T;
  iA12T = [iA(  (1:n),n+(1:n)) zeros(n,1); zeros(1,n+1)]*T;
  iA21T = [iA(n+(1:n),  (1:n)) zeros(n,1); zeros(1,n+1)]*T;
  iA22T = [iA(n+(1:n),n+(1:n)) zeros(n,1); zeros(1,n+1)]*T;

  a1u2 = conv2mat(ia(:,1), iu(:,2));
  a2u2 = conv2mat(ia(:,2), iu(:,2));

  itildeY1 = ir0*l1nu(iA12T*iu(:,1) + iA11T*alphu2 + iA12T*betau2,inu);
  itildeY1 = itildeY1 + (iL + ir0)*2*abs(imu)*ir0*(...
      l1nu(iA11T*a1u2,inu) + ...
      l1nu(iA11T*a2u2,inu) + ...
      l1nu(iA12T*a2u2,inu) + ...
      1.5*l1nu(iu(:,2),inu)*ir0 );

  itildeY2 = ir0*l1nu(iA22T*iu(:,1) + iA21T*alphu2 + iA22T*betau2,inu);
  itildeY2 = itildeY2 + (iL + ir0)*2*abs(imu)*ir0*(...
      l1nu(iA21T*a1u2,inu) + ...
      l1nu(iA21T*a2u2,inu) + ...
      l1nu(iA22T*a2u2,inu) + ...
      1.5*l1nu(iu(:,2),inu)*ir0 );

  itildeY = [ itildeY1 itildeY2 ];
end
