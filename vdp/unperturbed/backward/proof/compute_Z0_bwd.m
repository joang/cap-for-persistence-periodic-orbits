function Z0 = compute_Z0_bwd( N, inu, iB)
  n=N+1;
  wei=intval(ones(n,1));
  wei(2:end,1)=2*inu.^(1:N);

  Z0(1)= normm(iB(1:n,1:n), wei)+normm(iB(1:n,n+(1:n)), wei);
  Z0(2)= normm(iB(n+(1:n),1:n), wei)+normm(iB(n+(1:n),n+(1:n)), wei);
end

function norm=normm(iB, wei)
  norm = max(abs(iB')*wei./wei);
end
