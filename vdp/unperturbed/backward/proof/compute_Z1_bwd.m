function [Z1, iA] = compute_Z1_bwd( N, inu, iL, dc1, dc2, df, iB,alpha,beta)
  n=N+1;
  Lam=intval(2*diag((1:n)-1));
  R0=intval(zeros(n));
  R0(1,1)=intval('-1');
  R0(1,2:n)=intval(2*(2*mod(1:(n-1),2)-ones(1,n-1)));

  % create 2*(4*N+2)-dim DF
  R=intval(zeros(4*N+2));
  R(1,(N+2):end)=intval(2*(2*mod((N+1):(4*N+1),2)-ones(1,3*N+1)));
  T=intval(diag([0;ones(4*N,1)],1)-diag(ones(4*N+1,1),-1));
  lalpha=dc1;
  lbeta=dc2;
  DF=[R -iL*T*lalpha; -iL*T R-iL*T*lbeta];

  %A dagger without R and Lambda
  Adag=intval(zeros(2*(4*N+2)));
  % Adag(1:(N+1),1:(N+1))=df(1:(N+1),1:(N+1));
  Adag(1:(N+1),(4*N+3):(5*N+3))=df(1:(N+1),(N+2):(2*N+2));
  Adag((4*N+3):(5*N+3),1:(N+1))=df((N+2):(2*N+2), 1:(N+1));
  Adag((4*N+3):(5*N+3),(4*N+3):(5*N+3))=df((N+2):(2*N+2),(N+2):(2*N+2))-Lam-R0;

  %DF- A dagger
  DFAdag=DF-Adag;
  %matrix A, finite part
  iA=intval(zeros(2*(4*N+2)));
  iA(1:(N+1),1:(N+1))=iB(1:(N+1),1:(N+1));
  iA(1:(N+1),(4*N+3):(5*N+3))=iB(1:(N+1),(N+2):(2*N+2));
  iA((4*N+3):(5*N+3),1:(N+1))=iB((N+2):(2*N+2), 1:(N+1));
  iA((4*N+3):(5*N+3),(4*N+3):(5*N+3))=iB((N+2):(2*N+2),(N+2):(2*N+2));
  %tail of A
  iA((N+2):(4*N+2),(N+2):(4*N+2))=diag((1./intval(2*((N+1):(4*N+1)))));
  iA((5*N+4):end,(5*N+4):end)=diag((1./intval(2*((N+1):(4*N+1)))));

  iM=iA*DFAdag;

  Z1(1)= normm(iM(1:(4*N+2),1:(4*N+2)),inu)+normm(iM(1:(4*N+2),(4*N+3):end),inu);
  Z1(1)=Z1(1)+(normm(iB(1:(N+1),1:(N+1)),inu)+...
      normm(iB(1:(N+1),(N+2):(2*N+2)),inu))/inu^(4*N+1)+iL*(inu+1/inu)/(8*N);

  Z1(2)= normm(iM((4*N+3):end,1:(4*N+2)),inu)+normm(iM((4*N+3):end,(4*N+3):end),inu);
  Z1(2)=Z1(2)+(normm(iB((N+2):(2*N+2), 1:(N+1)),inu)...
      +normm(iB((N+2):(2*N+2),(N+2):(2*N+2)),inu))/inu^(4*N+1);
  Z1(2)=Z1(2)+(iL*(inu+1/inu)*(l1nu(alpha,inu)+l1nu(beta,inu)))/(2*(4*N+1));
end

function norm=normm(iB,inu)
  M=size(iB,1);
  wei=intval(ones(M,1));
  wei(2:end,1)=2*inu.^(1:(M-1));
  norm = max(abs(iB)'*wei./wei);
end
