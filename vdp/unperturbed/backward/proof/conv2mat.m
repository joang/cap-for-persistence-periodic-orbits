function [ab, A] = conv2mat(a,b)
 m = length(a); 
 A = toeplitz(a) + [zeros(m-1,1) hankel(a(2:end)); zeros(1,m)];
 ab= A*b;
end

