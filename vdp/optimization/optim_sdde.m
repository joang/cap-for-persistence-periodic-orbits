function [vals, objval, consta] = optim_sdde(fin, r0, ir0, dr0, idr0, dp0, idp0)
  %optim_sdde Optimization of the perturbative parameters
  %   

  file=fopen(fin, 'r');

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  omg0     = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  ProjPerp = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  ProjTop  = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  InvProj  = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  D2f      = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  DfK      = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  D2fK     = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  DK0      = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  DK       = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  D2K      = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  Fwd1     = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  Fwd      = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  Fwd1Bwd  = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  DFwd1Bwd = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  FwdBwd   = infsup(a(1), a(2));

  a=fscanf(file, '%*[^(]%*c%lf , %lf', 2);
  DFwdBwd  = infsup(a(1), a(2));

  mu=fscanf(file, '%*[^=]%*c%*[^=]%*c%lf', 1);
  D3f = 2*sqrt(intval('3'))*abs(intval(mu));

  fclose(file);

  % R=intval('76151');%infsup(eps,10);
  % DR=intval('10');%infsup(eps,10);
  P=intval('1');%infsup(eps,1);
  % DP=intval('1');%infsup(eps,1);

  C11 = Fwd1Bwd;
  C12 = FwdBwd;
  C13 = Fwd;

  C21 = 1 + Fwd1 + DFwd1Bwd;
  C22 = 1 + Fwd + DFwdBwd;

  M = InvProj*ProjPerp / omg0;

  % Q coefficients 3 (epsi,a,b0,b1,b2,dp,dr,r)
  Q_10000000 = C11 * P * ProjTop / DK0;
  Q_01100000 = C21 * ProjTop / DK0;
  Q_00200000 = C11 * D2f/2 * ProjTop / DK0;

  % P0 coefficients 4
  P0_10000000 = (M * C13 * C11 + C12 / omg0)*P;
  P0_01000000 = C12/omg0*DK;
  P0_01100000 = M * C13 * C21 + C22 / omg0;
  P0_00200000 = (M * C13 * C11 + C12 / omg0)*D2f/2;

  % P1 coefficients 5
  P1_10000000 = P / omg0;
  P1_01000000 = DK / omg0;
  P1_00100000 = DfK / omg0;
  P1_01010000 = 1 / omg0;
  P1_00200000 = D2f / 2 / omg0;

  % P2 coefficients 15
  P2_11020110 = 1 / omg0;
  P2_11010110 = 2*DK / omg0;
  P2_11000110 = DK*DK / omg0;
  P2_10020110 = intval('1');
  P2_10010110 = 2*DK;
  P2_10010100 = 1 / omg0;
  P2_10000110 = DK*DK;
  P2_10000100 = DK / omg0;
  P2_01001000 = 1 / omg0;
  P2_01000000 = D2K / omg0;
  P2_00210000 = D3f / omg0;
  P2_00200000 = D3f*DK / omg0;
  P2_00110000 = 2*D2f / omg0;
  P2_00100000 = D2fK*DK / omg0;
  P2_00010000 = DfK / omg0;

  % mu1 coefficients 9
  mu1_11010110 = ( C11*ProjTop)/DK0;
  mu1_11000110 = C11*DK*ProjTop/DK0;
  mu1_10010110 = ( C11*omg0*ProjTop)/DK0;
  mu1_10010101 = ( C11*ProjTop)/DK0;
  mu1_10000110 = C11*DK*omg0*ProjTop/DK0;
  mu1_10000101 = C11*DK*ProjTop/DK0;
  mu1_10000100 = ( C11*ProjTop)/DK0;
  mu1_01000000 = ( C21*ProjTop)/DK0;
  mu1_00100000 = ((C21 + C11*D2f)*ProjTop)/DK0;

  % mu2 coefficients 9
  mu2_11010110 = C12/omg0 + C11*C13*M + C11*C12*DK*ProjTop/(DK0*omg0);
  mu2_11000110 = DK*(C11*C13*DK0*M*omg0 + C12*(DK0 + C11*DK*ProjTop))/(DK0*omg0);
  mu2_10010110 = C12 + C11*C13*M*omg0 + C11*C12*DK*ProjTop/DK0;
  mu2_10010101 = (C12*DK0 + C11*C13*DK0*M*omg0 + C11*C12*DK*ProjTop)/(DK0*omg0);
  mu2_10000110 = DK*(C12 + C11*C13*M*omg0 + C11*C12*DK*ProjTop/DK0);
  mu2_10000101 = (DK*(C11*C13*DK0*M*omg0 + C12*(DK0 + C11*DK*ProjTop)))/(DK0*omg0);
  mu2_10000100 = (C12*DK0 + C11*C13*DK0*M*omg0 + C11*C12*DK*ProjTop)/(DK0*omg0);
  mu2_01000000 = (C22*DK0 + C13*C21*DK0*M*omg0 + C12*C21*DK*ProjTop)/(DK0*omg0);
  mu2_00100000 = (C22*DK0 + C13*(C21 + C11*D2f)*DK0*M*omg0 + ...
  C12*(D2f*DK0 + C21*DK*ProjTop + C11*D2f*DK*ProjTop))/(DK0*omg0);


  a = optimvar('a', 'LowerBound', eps, 'UpperBound', 1e-1);
  C0 = optimvar('C0', 'LowerBound', eps, 'UpperBound', 1e-1);
  C1 = optimvar('C1', 'LowerBound', eps, 'UpperBound', 5);
  C2 = optimvar('C2', 'LowerBound', eps, 'UpperBound', 5);
  epsi = optimvar('epsi', 'LowerBound', eps);
  R = optimvar('R', 'LowerBound', ir0.inf, 'UpperBound', ir0.sup);
  DR = optimvar('DR', 'LowerBound', idr0.inf, 'UpperBound', idr0.sup);
  DP = optimvar('DP', 'LowerBound', idp0.inf, 'UpperBound', idp0.sup);
  prob = optimproblem('ObjectiveSense','max');
  prob.Objective = epsi;%R;%*R*DR*DP^1

  prob.Constraints.cons1 = Q_10000000.sup*epsi + Q_01100000.sup*a*C0 ...
      + Q_00200000.sup*C0*C0 - a <= 0;

  prob.Constraints.cons2 = P0_10000000.sup*epsi + P0_01000000.sup*a ...
      + P0_01100000.sup*a*C0 + P0_00200000.sup*C0*C0 - C0 <= 0;

  prob.Constraints.cons3 = P1_10000000.sup*epsi + P1_01000000.sup*a  ...
      + P1_00100000.sup*C0 + P1_01010000.sup*a*C1 + P1_00200000.sup*C0*C0 - C1 <= 0;

  prob.Constraints.cons4 = P2_11020110.sup*epsi*a*C1*C1*DP*DR + P2_11010110.sup*epsi*a*C1*DP*DR ...
      + P2_11000110.sup*epsi*a*DP*DR + P2_10020110.sup*epsi*C1*C1*DP*DR ...
      + P2_10010110.sup*epsi*C1*DP*DR + P2_10010100.sup*epsi*C1*DP ...
      + P2_10000110.sup*epsi*DP*DR + P2_10000100.sup*epsi*DP ... 
      + P2_01001000.sup*a*C2 + P2_01000000.sup*a ...
      + P2_00210000.sup*C0*C0*C1 + P2_00200000.sup*C0*C0 ...
      + P2_00110000.sup*C0*C1 + P2_00100000.sup*C0 + P2_00010000.sup*C1 - C2 <= 0;

  prob.Constraints.cons5 = mu1_11010110.sup*epsi*a*C1*DP*DR + ...
      mu1_11000110.sup*epsi*a*DP*DR + mu1_10010110.sup*epsi*C1*DP*DR + ...
      mu1_10010101.sup*epsi*C1*DP*R + mu1_10000110.sup*epsi*DP*DR + ...
      mu1_10000101.sup*epsi*DP*R + mu1_10000100.sup*epsi*DP + ...
      mu1_01000000.sup*a + mu1_00100000.sup*C0 -1 + eps <= 0;

  prob.Constraints.cons6 = mu2_11010110.sup*epsi*a*C1*DP*DR + ...
      mu2_11000110.sup*epsi*a*DP*DR + mu2_10010110.sup*epsi*C1*DP*DR + ...
      mu2_10010101.sup*epsi*C1*DP*R + mu2_10000110.sup*epsi*DP*DR + ...
      mu2_10000101.sup*epsi*DP*R + mu2_10000100.sup*epsi*DP + ...
      mu2_01000000.sup*a + mu2_00100000.sup*C0 - 1 +eps <= 0;

  x0.a = 1e-2; 
  x0.C0 = 1e-2; 
  x0.C1 = 5e-1; 
  x0.C2 = 5e-1;
  x0.epsi = 1e-2;
  x0.DP = dp0;
  x0.DR = dr0;
  x0.R = r0;
  [sol, objval] = solve(prob,x0);
  % sol
  % objval

  % Checking the inequalities with the interval
  ia = intval(sol.a);
  iC0 = intval(sol.C0);
  iC1 = intval(sol.C1);
  iC2 = intval(sol.C2);
  iDP = infsup(eps,sol.DP);
  iDR = infsup(eps,sol.DR);
  iR = infsup(eps,sol.R);
  iepsi = infsup(eps,sol.epsi);

  icons1 = Q_10000000*iepsi + Q_01100000*ia*iC0 ...
      + Q_00200000*iC0*iC0 - ia <= 0;

  icons2 = P0_10000000*iepsi + P0_01000000*ia ...
      + P0_01100000*ia*iC0 + P0_00200000*iC0*iC0 - iC0 <= 0;

  icons3 = P1_10000000*iepsi + P1_01000000*ia ...
      + P1_00100000*iC0 + P1_01010000*ia*iC1 ...
      + P1_00200000*iC0*iC0 - iC1 <= 0;

  icons4 = P2_11020110*iepsi*ia*iC1*iC1*iDP*iDR ...
      + P2_11010110*iepsi*ia*iC1*iDP*iDR ...
      + P2_11000110*iepsi*ia*iDP*iDR + P2_10020110*iepsi*iC1*iC1*iDP*iDR ...
      + P2_10010110*iepsi*iC1*iDP*iDR + P2_10010100*iepsi*iC1*iDP ...
      + P2_10000110*iepsi*iDP*iDR + P2_10000100*iepsi*iDP ... 
      + P2_01001000*ia*iC2 + P2_01000000*ia ...
      + P2_00210000*iC0*iC0*iC1 + P2_00200000*iC0*iC0 ...
      + P2_00110000*iC0*iC1 + P2_00100000*iC0 + P2_00010000*iC1 - iC2 <= 0;

  icons5 = mu1_11010110*iepsi*ia*iC1*iDP*iDR + ...
      mu1_11000110*iepsi*ia*iDP*iDR + mu1_10010110*iepsi*iC1*iDP*iDR + ...
      mu1_10010101*iepsi*iC1*iDP*iR + mu1_10000110*iepsi*iDP*iDR + ...
      mu1_10000101*iepsi*iDP*iR + mu1_10000100*iepsi*iDP + ...
      mu1_01000000*ia + mu1_00100000*iC0 -1 < 0;
  icons6 = mu2_11010110*iepsi*ia*iC1*iDP*iDR + ...
      mu2_11000110*iepsi*ia*iDP*iDR + mu2_10010110*iepsi*iC1*iDP*iDR + ...
      mu2_10010101*iepsi*iC1*iDP*iR + mu2_10000110*iepsi*iDP*iDR + ...
      mu2_10000101*iepsi*iDP*iR + mu2_10000100*iepsi*iDP + ...
      mu2_01000000*ia + mu2_00100000*iC0 - 1 < 0;

  consta = icons1 && icons2 && icons3 && icons4 && icons5 && icons6;

  vals.a  = sol.a;
  vals.C0 = sol.C0;
  vals.C1 = sol.C1;
  vals.C2 = sol.C2;
  vals.DP = sol.DP;
  vals.DR = sol.DR;
  vals.R  = sol.R;
  vals.epsi = sol.epsi;
  vals.mu = mu;

  fprintf('constraints= %d\n', consta);
end

