   clear; close all;

% Filename with the interval bounds
fin = "";

r0 = 1;  ir0  = infsup(r0, r0);
dr0 = 1; idr0 = infsup(dr0, dr0);
dp0 = 1; idp0 = infsup(dp0, dp0);

[vals, objval, consta] = optim_sdde(fin, r0, ir0, dr0, idr0, dp0, idp0);
