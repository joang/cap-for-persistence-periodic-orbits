function y = cosacos(c,x)
  % X should be a row vector.
  if ( size(x, 1) > 1 )
      warning('cosacos:xDim', ...
          'Evaluation points should be a row vector.');
      x = x(:);
  end

  if (isintval(x) || isintval(c))
      x = intval(x);
      c = intval(c);
  end

  if (size(c,2) == 1 && max(size(x)) == 1) 
      y = cosacos_scl(c,x)';
  else
      y = cosacos_vec(c,x);
  end
end

function y = cosacos_scl(c,x)
  n = size(c,1)-1;
  Tk= cos((1:n)*acos(x));
  y = c(1) + 2*Tk*c(2:end);
end

function y = cosacos_vec(c,x)
  [n,d] = size(c); n=n-1;
  m = size(x,2);
  y = 0*repmat(x,d,1);%zeros(d,m)*cast;
  for k = 1:m
      Tk = cos((1:n).*acos(x(k)));
      y(:,k) = c(1,:) + 2*Tk*c(2:end,:);
  end
end

