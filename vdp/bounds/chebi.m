classdef chebi
    %chebi Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = public)
        coefs
    end
    
    methods ( Access = public, Static = false )
        function obj = chebi(c)
            % Constructor
            obj.coefs = c;
        end
        
        function obj = plus(a,b)
            % addition
%             n = min(length(a),length(b));
            aisa = isa(a,'chebi');
            bisa = isa(b,'chebi');
            if (aisa && bisa)
                n = max(size(a.coefs,1), size(b.coefs,1));
                obj = padi(a.coefs,n) + padi(b.coefs,n);
            elseif (aisa && ~bisa)
                obj = a.coefs;
                obj(1) = obj(1) + b;
            elseif (~aisa && bisa)
                obj = b.coefs;
                obj(1) = obj(1) + a;
            end
            obj = chebi(obj);
        end
        
        function obj = minus(a,b)
            % addition
            aisa = isa(a,'chebi');
            bisa = isa(b,'chebi');
            if (aisa && bisa)
                n = max(size(a.coefs,1), size(b.coefs,1));
                obj = padi(a.coefs,n) - padi(b.coefs,n);
            elseif (aisa && ~bisa)
                obj = a.coefs;
                obj(1) = obj(1) - b;
            elseif (~aisa && bisa)
                obj = - b.coefs;
                obj(1) = obj(1) + a;
            else
                error('chebi:plus:unknown', ... 
                    ['Undefined function ''plus'' for input arguments of type ' ...
                    '%s and %s.'], class(a), class(b));
            end
            obj = chebi(obj);
        end
        
        function obj = mtimes(a,b)
            % product
            aisa = isa(a,'chebi');
            bisa = isa(b,'chebi');
            if (aisa && bisa)
                n = max(size(a.coefs,1), size(b.coefs,1));
                obj = convmat(padi(a.coefs,n),padi(b.coefs,n));
            elseif (aisa && ~bisa)
                obj = a.coefs*b;
            elseif (~aisa && bisa)
                obj = a*b.coefs;
            else
                error('chebi:mtimes:unknown', ... 
                    ['Undefined function ''mtimes'' for input arguments of type ' ...
                    '%s and %s.'], class(a), class(b));
            end
            obj = chebi(obj);
        end
        
        function d = norm(a,varargin)
            % norm
            if nargin > 1
                inu = varargin{1};
            else
                inu = 1;
            end
            d = l1nu(a.coefs, inu);
        end
        
        function obj=subsref(a,x)
            switch x(1).type
                case '.'
                    if strncmp(x(1).subs, 'coefs',5)
                        obj = a.coefs;
                    end
                case'()'
                    if numel(x(1).subs) == 1
                        obj = cosacos(a.coefs, x(1).subs{:});
                    elseif numel(x(1).subs) == 2
                        obj = a.coefs(x(1).subs{:});
                    end
                case'{}'
                    d=length(x(1).subs);
                    for k = 1:d
                        obj(k)=chebi(a.coefs(:,x(1).subs{k}));
                    end
                otherwise
                    error('chebi:subsref:unknown', ...
                        'Not a valid indexing expression');
            end
            if length(x) > 1
                obj = subsref(obj,x(2:end));
            end
        end
        
        function obj=size(a)
            obj=size(a.coefs);
        end
        
        function obj=length(a)
            obj=length(a.coefs);
        end 
        
        function obj=abs(a)
            obj= abs(a.coefs);
        end  
        
        function obj = sup(a)
            obj = chebi(sup(a.coefs));
        end  
    end
    
    methods (Access = public, Static = true)
        function obj=prod(varargin)
            for k=1:nargin
                varargin{k} = varargin{k}.coefs;
            end
            obj=chebi(convmat(varargin{:}));
        end
    end
end

function b=padi(a,padsize,varargin)
if nargin > 2
    padval = varargin{1};
else
    padval = 0;
end
[n,d] = size(a); assert(padsize >= n);
b = [a; repmat(padval,padsize-n,d)];
end
