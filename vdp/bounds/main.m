
%clear
if ~exist('mu','var')
    mu = 1;
end
load(sprintf('../po/proof/proof0-mu_%g.mat',mu));
load(sprintf('../forward/proof/proof1-mu_%g.mat',mu));
load(sprintf('../backward/proof/proof2-mu_%g.mat',mu));
[n,d] = size(a);

u(abs(u)<eps)=0;
imu = intval(mu); btmu = bartilde(imu, intval('0'));
inu = intval(nu0);
ir0 = intval(r0);
ir1 = intval(r1);
ir2 = intval(r2);
iL = intval(L); btL = bartilde(iL, ir0);
ia = intval(a); ca = chebi(ia); bta = bartilde(ca, repmat(ir0,1,size(a,2)));
iv = intval(v); cv = chebi(iv); btv = bartilde(cv, repmat(ir1,1,size(v,2)));
iu = intval(u); cu = chebi(iu); btu = bartilde(cu, repmat(ir2,1,size(u,2)));


M = reshape(cosacos(iv,intval('1')),d,d);
Mr1 = M + infsup(-r1,r1);

%% rigorous eigenpairs of the monodromy matrix
[U,D]=eig(mid(Mr1));
vaps = intval(zeros(d,1));
veps = intval(zeros(d));
for i=1:d
    [vap,vep]=verifyeig(Mr1,D(i,i),U(:,i));
    vaps(i) = vap;
    veps(:,i) = vep;
end

%% Projections
alpha=veps(:,1)'*veps(:,2)/(norm(veps(:,1))*norm(veps(:,2)));
alpha=sin(acos(alpha));
ProjTop=1/alpha;
ProjPerp = ProjTop;

%% (Id - M)^-1
[~,idx] = max(mid(abs(1-vaps)));
InvProj = 1 / (1 - vaps(idx));

%% Bound of D2f
xmin = verifyglobalmin(@(x)  cosacos(a(:,1),x), infsup(-1,1),verifyoptimset('boxes',56));
xmax = verifyglobalmin(@(x) -cosacos(a(:,1),x), infsup(-1,1),verifyoptimset('boxes',56));
xmax = max(abs(xmin),abs(xmax));

ymin = verifyglobalmin(@(x)  cosacos(a(:,2),x), infsup(-1,1),verifyoptimset('boxes',56));
ymax = verifyglobalmin(@(x) -cosacos(a(:,2),x), infsup(-1,1),verifyoptimset('boxes',56));
ymax = max(abs(ymin),abs(ymax));

D2f = 2*abs(imu)*(2*(sup(xmax)+ir0))+sup(ymax)+ir0;

%% Bound of |Df(K(t))|

id = bartilde(chebi(intval(eye(n,1))),intval('0'));
DfK1 = -2*btmu*bartilde.prod3(bta{1},bta{2},id)-id;
DfK2 = btmu*(id-bartilde.prod3(bta{1},bta{1},id));
DfK1 = btL*DfK1;
DfK2 = btL*DfK2;

DfK = norm(btL)^2 + norm(DfK1)^2 + norm(DfK2)^2;
DfK = sqrt(DfK);

%% Bound of |D2f(K(t))|

D2fK = 2*norm(btmu)*(2*norm(bta{1})+norm(bta{2}));

%% Bound of |DK(t)|
DK1 = bta{2};
DK2 = btmu * (bta{2} - bartilde.prod3(bta{1}, bta{1}, bta{2})) - bta{1};

DK = norm(DK1)^2 + norm(DK2)^2;
DK = sqrt(DK);

%% Bound for |DK(-1)|
DK0 = sqrt(norm(DK1(intval('-1')))^2 + norm(DK2(intval('-1')))^2);

%% Bound of |D2K(t)|
D2K1 = btL * DK2;
% alpha(a2)
D2K2 = -2*btmu * bartilde.prod3(bta{1}, bta{2}, bta{2}) - bta{2};
D2K2 = D2K2 + ...
    btmu*(DK2 - ...
          (btmu * (bartilde.prod3(bta{1}, bta{1}, bta{2}) - ...
           bartilde.prod5(bta{1}, bta{1}, bta{1}, bta{1}, bta{2})) - ...
           bartilde.prod3(bta{1}, bta{1}, bta{1})));
D2K = norm(D2K1)^2 + norm(D2K2)^2;
D2K = sqrt(D2K);

% D2K1 = btL * DK2;
% D2K2 = DfK1 * DK1 + DfK2 * DK2;
% 
% D2K = norm(D2K1)^2 + norm(D2K2)^2;
% D2K = sqrt(D2K);

%% Bound for |F(1)|
M = btv(intval('1'));
Fwd1 = sqrt(norm(M{1})^2 + norm(M{2})^2 + norm(M{3})^2 + norm(M{4})^2); 

%% Bound for |F(t)|
Fwd = sqrt(norm(btv{1})^2 + norm(btv{2})^2 + norm(btv{3})^2 + norm(btv{4})^2);


%% Bound of |Phi(1; s)|=|F(1) B(s)| 
% u is in col-major
% M = btv(intval('1'));
FB11 = M{1}*btu{1} + M{3}*btu{3}; aux = norm(FB11)^2; Mu = aux;
FB12 = M{1}*btu{2} + M{3}*btu{4}; aux = norm(FB12)^2; Mu = Mu + aux;
FB21 = M{2}*btu{1} + M{4}*btu{3}; aux = norm(FB21)^2; Mu = Mu + aux;
FB22 = M{2}*btu{2} + M{4}*btu{4}; aux = norm(FB22)^2; Mu = Mu + aux;
Fwd1Bwd = sqrt(Mu);

%% Bound of |d/dt Phi(1;t)| = |- L F(1) B(t) Df(K(t))|
aux = -2*btmu*bartilde.prod3(bta{1},bta{2},FB12)-FB12; aux = btL * aux;
aux = norm(aux)^2; DFwd1Bwd = aux;

aux = btmu*(FB12-bartilde.prod3(bta{1},bta{1},FB12)); aux = btL * (aux+FB11);
aux = norm(aux)^2; DFwd1Bwd = DFwd1Bwd + aux;

aux = -2*btmu*bartilde.prod3(bta{1},bta{2},FB22)-FB22; aux = btL * aux;
aux = norm(aux)^2; DFwd1Bwd = DFwd1Bwd + aux;

aux = btmu*(FB22-bartilde.prod3(bta{1},bta{1},FB22)); aux = btL * (aux+FB21);
aux = norm(aux)^2; DFwd1Bwd = DFwd1Bwd + aux;

DFwd1Bwd = sqrt(DFwd1Bwd);

%% alpha11
alph11 = sup(Fwd1Bwd*D2f*0.5);

%% alpha12
alpha12 = 0;

%% Bound of |Phi(t;s)| = |Phi(t)Phi^-1(s)|=|F(t) B(s)| for s<=t
if ~exist('nt','var')
    nt = 500;
end
FwdBwd = 0;
for k = 1:nt 
    tk = infsup(k-1,k)/nt;    
    V = btv(2*tk-1);
    
    sj = infsup((1:k)-1, 1:k).*tk.sup./k;
    U = btu(2.*sj-1);
    
    a11 = V{1}*U{1} + V{3}*U{3};
    a12 = V{1}*U{2} + V{3}*U{4};  
    a21 = V{1}*U{1} + V{3}*U{3};
    a22 = V{1}*U{2} + V{3}*U{4};
    fronorm = max(sqrt(norm(a11).^2 + norm(a12).^2 + ...
                       norm(a21).^2 + norm(a22).^2));
    FwdBwd = max(FwdBwd, fronorm); % reduce variable.
end

%% Bound of |d/ds Phi(t;s)| = |F(t) d/ds B(s)| = |- L F(t) B(s) Df(K(s))| for s<=t
if ~exist('nt','var')
    nt = 500;
end
DBwd11 = -2*btmu*bartilde.prod3(bta{1},bta{2},btu{2}) - btu{2}; 
DBwd12 = btu{1} + btmu*(btu{2}-bartilde.prod3(bta{1},bta{1},btu{2}));
DBwd21 = -2*btmu*bartilde.prod3(bta{1},bta{2},btu{4}) - btu{4};
DBwd22 = btu{3} + btmu*(btu{4}-bartilde.prod3(bta{1},bta{1},btu{4}));

DBwd11 = btL * DBwd11; DBwd12 = btL * DBwd12;
DBwd21 = btL * DBwd21; DBwd22 = btL * DBwd22;
DFwdBwd = 0;
for k = 1:nt
    tk = infsup(k-1,k)/nt;   
    V = btv(2*tk-1);
    
    sj = infsup((1:k)-1, 1:k).*tk.sup./k;
    U = btu(2*sj-1);
    b11 = DBwd11(2*sj-1); b12 = DBwd12(2*sj-1);
    b21 = DBwd21(2*sj-1); b22 = DBwd22(2*sj-1);
        
    a11 = V{1}*b11 + V{3}*b21;
    a12 = V{1}*b12 + V{3}*b22;
    a21 = V{2}*b11 + V{4}*b21;
    a22 = V{2}*b12 + V{4}*b22;
      
    fronorm = max(sqrt(norm(a11).^2 + norm(a12).^2 + ...
                       norm(a21).^2 + norm(a22).^2));
    DFwdBwd = max(DFwdBwd, fronorm);
end

%% Print of values
omg = norm(2*btL);
fprintf('omg0     = infsup(% .15e , % .15e);\n', inf(omg), sup(omg));
fprintf('ProjPerp = infsup(% .15e , % .15e);\n', inf(ProjPerp), sup(ProjPerp));
fprintf('ProjTop  = infsup(% .15e , % .15e);\n', inf(ProjTop), sup(ProjTop));
fprintf('InvProj  = infsup(% .15e , % .15e);\n', inf(InvProj), sup(InvProj));

fprintf('D2f      = infsup(% .15e , % .15e);\n', inf(D2f), sup(D2f));
fprintf('DfK      = infsup(% .15e , % .15e);\n', inf(DfK), sup(DfK));
fprintf('D2fK     = infsup(% .15e , % .15e);\n', inf(D2fK), sup(D2fK));
fprintf('DK0      = infsup(% .15e , % .15e);\n', inf(DK0), sup(DK0));
fprintf('DK       = infsup(% .15e , % .15e);\n', inf(DK), sup(DK));
fprintf('D2K      = infsup(% .15e , % .15e);\n', inf(D2K), sup(D2K));

fprintf('Fwd1     = infsup(% .15e , % .15e);\n', inf(Fwd1), sup(Fwd1));
fprintf('Fwd      = infsup(% .15e , % .15e);\n', inf(Fwd), sup(Fwd));
fprintf('Fwd1Bwd  = infsup(% .15e , % .15e);\n', inf(Fwd1Bwd), sup(Fwd1Bwd));
fprintf('DFwd1Bwd = infsup(% .15e , % .15e);\n', inf(DFwd1Bwd), sup(DFwd1Bwd));
fprintf('FwdBwd   = infsup(% .15e , % .15e);\n', inf(FwdBwd), sup(FwdBwd));
fprintf('DFwdBwd  = infsup(% .15e , % .15e);\n', inf(DFwdBwd), sup(DFwdBwd));
fprintf('nt       = %d\n', nt);
fprintf('mu       = % .15e\n', mu);
