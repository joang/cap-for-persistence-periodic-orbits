function [a,A] = convmat(varargin) 
% convmat(a1,a2,a3)  A = ell_{a1*a2}=ell_{a1}*ell_{a2} and a = a1*a2*a3
% ell_{a1*a2}(a3) =a  and a = a1*a2*a3
m = nargin-1;
nn = arrayfun(@(x) size(x{1},1),varargin);
maxn = max(nn);
A = 1;
for k = 1:m
    varargin{k} = [varargin{k}; zeros(maxn*(m+1)-nn(k), 1)];
    A = A*mat(varargin{k});
end
varargin{end} = [varargin{end}; zeros(maxn*(m+1)-nn(end), 1)];
a = A*varargin{end};
end

function A=mat(a)
m=length(a);
A = toeplitz(a) + [zeros(m-1,1) hankel(a(2:end)); zeros(1,m)];
end
 
