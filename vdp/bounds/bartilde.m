classdef bartilde
    % Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = public)
        bar
        tilde
    end
    
    methods ( Access = public, Static = false )      
        function obj = bartilde(varargin)
            % constructor
            if nargin == 1
                obj.bar = varargin{1};
                obj.tilde = norm(obj.bar);
                assert(sum((obj.tilde >= 0)-1) == 0);
            elseif nargin == 2
                obj.bar = varargin{1};
                obj.tilde = varargin{2};
                assert(sum((obj.tilde >= 0)-1) == 0);
            else
                error('chebi:constructor:unknown', ... 
                    ['Undefined function ''bartilde'' for input arguments of type ' ...
                    '%d.'], nargin);
            end
        end
        
        function obj = plus(a,b)
            % addition
            aisa = isa(a,'bartilde');
            bisa = isa(b,'bartilde');
            if (aisa && bisa)
                ab = a.bar + b.bar;
                abr = a.tilde + b.tilde;
            elseif (aisa && ~bisa)
                ab = a.bar + b;
                abr = a.tilde;
            elseif (~aisa && bisa)
                ab = a + b.bar;
                abr = b.tilde;
            else
                error('bartilde:plus:unknown', ... 
                    ['Undefined function ''plus'' for input arguments of type ' ...
                    '%s and %s.'], class(a), class(b));
            
            end
            obj = bartilde(ab, abr);
        end
        
        function obj = minus(a,b)
            % addition
            aisa = isa(a,'bartilde');
            bisa = isa(b,'bartilde');
            if (aisa && bisa)
                ab = a.bar - b.bar;
                abr = norm(a.tilde - b.tilde);
            elseif (aisa && ~bisa)
                ab = a.bar - b;
                abr = a.tilde;
            elseif (~aisa && bisa)
                ab = a - b.bar;
                abr = b.tilde;
            else
                error('bartilde:minus:unknown', ... 
                    ['Undefined function ''plus'' for input arguments of type ' ...
                    '%s and %s.'], class(a), class(b));
            
            end
            obj = bartilde(ab, abr);
        end
        
        function obj = mtimes(a,b)
            % multiplication            
            aisa = isa(a,'bartilde');
            bisa = isa(b,'bartilde');
            if (aisa && bisa)
                if isa(b.bar, 'chebi')
                    % 
                    ab = b.bar * a.bar;
                    abr = a.tilde*(norm(b.bar) + b.tilde) + norm(a.bar)*b.tilde;
                elseif ~isa(a.bar, 'chebi') && ~isa(b.bar, 'chebi')
                    ab = a.bar * b.bar;
                    abr = a.tilde*(abs(b.bar) + b.tilde) + abs(a.bar)*b.tilde;
                else
                    assert(0, 'to implement');
                end
            elseif (aisa && ~bisa)
                ab = a.bar * b;
                abr = a.tilde*norm(b);
            elseif (~aisa && bisa)
                ab = a * b.bar;
                abr = b.tilde*norm(a);
            else
                error('bartilde:mtimes:unknown', ... 
                    ['Undefined function ''mtimes'' for input arguments of type ' ...
                    '%s and %s.'], class(a), class(b));
            
            end
            obj = bartilde(ab, abr);
        end
        
        function obj = mpower(a,b)
            if b == 2
                obj = a*a;
            end
        end
        
        function obj=subsref(a,x)            
            switch x(1).type
                case '.'
                    if strncmp(x(1).subs, 'bar',3)
                        obj = a.bar;
                    elseif strncmp(x(1).subs, 'tilde',5)
                        obj = a.tilde;
                    end
                case '()'
                    if isa(a.bar, 'chebi')
                        b = a.bar(x(1).subs{:});
                        t = a.tilde;
                        obj = bartilde(b, t);
                    else
                        obj = a;
                    end
                case'{}'
                    d=length(x(1).subs);
                    if isa(a.bar,'chebi')
                        for k = 1:d
                            obj(k)=bartilde(a.bar{x(1).subs{k}}, ...
                                            a.tilde(x(1).subs{k}));
                        end
                    else
                        for k = 1:d
                            obj(k)=bartilde(a.bar(x(1).subs{k},:), ...
                                            a.tilde(x(1).subs{k}));
                        end
                    end
                otherwise
                    error('bartilde:subsref:unknown', ...
                        'Not a valid indexing expression');
            
            end
            if length(x) > 1
                obj = subsref(obj,x(2:end));
            end            
        end   
        
        function d = norm(a)
            if isa(a.bar, 'chebi')
                d = norm(a.bar) + a.tilde;
            else
                d = a.bar + a.tilde;
            end
        end
        
        function obj = sup(a)
            obj = bartilde(sup(a.bar),sup(a.tilde));
        end
        
        function obj = abs(a)
            obj = bartilde(abs(a.bar),abs(a.tilde));
        end
    end
    
    methods (Access = public, Static = true)
        function obj=prod2(a1,a2)
           obj = a1*a2;
        end
        
        function obj=prod3(a1,a2,a3)
            b=chebi.prod(a1.bar,a2.bar,a3.bar);
            t= a2.tilde*(norm(chebi.prod(a1.bar,a3.bar)) + ...
                a3.tilde*norm(a1.bar)) + ...
               norm(chebi.prod(a1.bar,a3.bar))*a3.tilde + ...
               a1.tilde*(a2.tilde*(norm(a3.bar) + a3.tilde) + ...
                        norm(a2.bar)*a3.tilde + ...
                        norm(chebi.prod(a2.bar,a3.bar)));
           obj = bartilde(b,t);
        end
        
        function obj=prod4(a1,a2,a3,a4)
            a2a1 = norm(chebi.prod(a1.bar,a2.bar));
            a3a1 = norm(chebi.prod(a1.bar,a3.bar));
            a4a1 = norm(chebi.prod(a1.bar,a4.bar));
            a3a2 = norm(chebi.prod(a2.bar,a3.bar));
            a4a2 = norm(chebi.prod(a2.bar,a4.bar));
            a4a3 = norm(chebi.prod(a3.bar,a4.bar));
            
            a3a2a1 = norm(chebi.prod(a1.bar,a2.bar,a3.bar));
            a4a2a1 = norm(chebi.prod(a1.bar,a2.bar,a4.bar));
            a4a3a1 = norm(chebi.prod(a1.bar,a3.bar,a4.bar));
            a4a3a2 = norm(chebi.prod(a2.bar,a3.bar,a4.bar));
            
            b = chebi.prod(a1.bar,a2.bar,a3.bar,a4.bar);
            
            t1 = norm(a1.bar) + a1.tilde; % ba1 + ta1
            t2 = a2.tilde * t1 + a2a1 + norm(a2.bar)*a1.tilde; % (ba2 + ta2)(ba1 + ta1)
            t3 = a3.tilde * t2 + a2.tilde * (a3a1 + norm(a3.bar)*a1.tilde) + a3a2a1 + a3a2*a1.tilde;  
            % (ba3 + ta3)(ba2 + ta2)(ba1 + ta1)
            t  = a4.tilde * t3 + ...
                a3.tilde*(a2.tilde*(a4a1 + norm(a4.bar)*a1.tilde) + a4a2a1 + a4a2*a1.tilde) + ...
                a2.tilde*(a4a3a1 + a4a3*a1.tilde) + ...
                a4a3a2*a1.tilde;
            % (ba4 + ta4)(ba3 + ta3)(ba2 + ta2)(ba1 + ta1)
           obj = bartilde(b,t);
        end
        
        function obj=prod5(a1,a2,a3,a4,a5)
            a2a1 = norm(chebi.prod(a1.bar,a2.bar));
            a3a1 = norm(chebi.prod(a1.bar,a3.bar));
            a4a1 = norm(chebi.prod(a1.bar,a4.bar));
            a5a1 = norm(chebi.prod(a1.bar,a5.bar));
            
            a3a2 = norm(chebi.prod(a2.bar,a3.bar));
            a4a2 = norm(chebi.prod(a2.bar,a4.bar));
            a5a2 = norm(chebi.prod(a2.bar,a5.bar));
            
            a4a3 = norm(chebi.prod(a3.bar,a4.bar));
            a5a3 = norm(chebi.prod(a3.bar,a5.bar));
            
            a5a4 = norm(chebi.prod(a4.bar,a5.bar));
            
            
            a3a2a1 = norm(chebi.prod(a1.bar,a2.bar,a3.bar));
            a4a2a1 = norm(chebi.prod(a1.bar,a2.bar,a4.bar));
            a5a2a1 = norm(chebi.prod(a1.bar,a2.bar,a5.bar));
            a4a3a1 = norm(chebi.prod(a1.bar,a3.bar,a4.bar));
            a5a3a1 = norm(chebi.prod(a1.bar,a3.bar,a5.bar));
            a5a4a1 = norm(chebi.prod(a1.bar,a4.bar,a5.bar));
            a4a3a2 = norm(chebi.prod(a2.bar,a3.bar,a4.bar));
            a5a3a2 = norm(chebi.prod(a2.bar,a3.bar,a5.bar));
            a5a4a2 = norm(chebi.prod(a2.bar,a4.bar,a5.bar));
            a5a4a3 = norm(chebi.prod(a3.bar,a4.bar,a5.bar));
            
            
            a4a3a2a1 = norm(chebi.prod(a1.bar,a2.bar,a3.bar,a4.bar));
            a5a3a2a1 = norm(chebi.prod(a1.bar,a2.bar,a3.bar,a5.bar));
            a5a4a2a1 = norm(chebi.prod(a1.bar,a2.bar,a4.bar,a5.bar));
            a5a4a3a1 = norm(chebi.prod(a1.bar,a3.bar,a4.bar,a5.bar));
            a5a4a3a2 = norm(chebi.prod(a2.bar,a3.bar,a4.bar,a5.bar));
            
            b=chebi.prod(a1.bar,a2.bar,a3.bar,a4.bar,a5.bar);
            
            t1 = norm(a1.bar) + a1.tilde;
            t2 = a2.tilde * t1 + a2a1 + norm(a2.bar)*a1.tilde;
            t3 = a3.tilde * t2 + a2.tilde * (a3a1 + norm(a3.bar)*a1.tilde) + a3a2a1 + a3a2*a1.tilde;
            t4 = a4.tilde * t3 + ...
                a3.tilde * (a2.tilde*(a4a1 + norm(a4.bar)*a1.tilde) + a4a2a1 + a4a2*a1.tilde) + ...
                a2.tilde * (a4a3a1 + a4a3*a1.tilde) + ...
                a4a3a2a1 + a4a3a2*a1.tilde;
            t  = a5.tilde * t4 + ...
                a4.tilde * (a3.tilde*(a2.tilde*(a5a1 + norm(a5.bar)*a1.tilde) + a5a2a1 + a5a2*a1.tilde) + ...
                a2.tilde * (a5a3a1 * a5a3*a1.tilde) + a5a3a2a1 + a5a3a2*a1.tilde) + ...
                a3.tilde * (a2.tilde*(a5a4a1 + a5a4*a1.tilde) + a5a4a2a1 + a5a4a2*a1.tilde) + ...
                a2.tilde * (a5a4a3a1 + a5a4a3*a1.tilde) + ...
                a5a4a3a2*a1.tilde;
           obj = bartilde(b,t);
        end
    end
end

