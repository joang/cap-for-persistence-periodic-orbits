# Persistence of Periodic Orbits under State-dependent Delayed Perturbations: Computer-assisted Proofs

## Description

The files in this project provides the code used in the preprint [Gimeno et al. (2021)](https://arxiv.org/abs/2111.06391)

It contains the different steps to rigorously proof the persistence of periodic orbits for state-dependent delayed perturbation of the Van de Pol equation. The algorithm is based on the results in [Yang et al. (2021)](https://arxiv.org/abs/2103.05203), which provides a-posteriori theorem for the existence of these kind of orbits under the generic assumption that the unpertubed periodic orbit has simple eigenvalue 1 in its monodromy matrix.

The code contains the following information:

 - Computer-assisted Proof for the unpertubed periodic orbit, see vdp > unpertubed > po.
 
 - Computer-assisted Proof for the forward variational flow around the periodic orbit, see vdp > unpertubed > forward.
 
 - Computer-assisted Proof for the backward variational flow around the periodic orbit, see vdp > unpertubed > backward.
 
 - Rigorous computation of the bounds, see vdp > bounds.
 
 - Optimization of parameters to provide explicit values for which the perturbed periodic orbit exits, see vdp > optimization.
 
 
## Software dependencies

 The code has been implemented and run it in MATLAB R2020b.
 The following packages are used:
 
 - chebfun to provide initial seeds of the periodic orbit.
 
 - INTLAB v11 for interval arithmetic and rigorous verifications.
 
 - Optimization Toolbox for parameter values optimization.
 
 - Curve Fitting Toolbox for the rigorous enclosure of convolutions.


